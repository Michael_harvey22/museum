<?php
$pageTitle = __('Browse Items');
echo head(array('title'=>$pageTitle,'bodyclass' => 'items browse'));
?>

<h1><?php echo $pageTitle;?> <?php echo __('(%s total)', $total_results); ?></h1>

<!-- The following code prints a secondary-nav on the browse Item page -->

<nav class="items-nav navigation secondary-nav">
    <?php echo public_nav_items(); ?>
</nav>

<?php echo item_search_filters(); ?>

<?php echo pagination_links(); ?>

<?php if ($total_results > 0): ?>

<!-- The criteria used for sorting items. -->
<?php
$sortLinks[__('Title')] = 'MUMAC,Title';
$sortLinks[__('Collection')] = 'MUMACElementSet,Collection';
$sortLinks[__('Date Added')] = 'added';
?>

<!-- echo the sorting options -->
<div id="sort-links">
    <span class="sort-label"><?php echo __('Sort by: '); ?></span><?php echo browse_sort_links($sortLinks); ?>
</div>
<!-- Sorting options end here --> 
<?php endif; ?>
<br><br><br><br>
<?php foreach (loop('items') as $item): ?>
<?php $tags = tag_string('item'); 
	if(!strpos($tags, 'slideshow')!== false):?>
	
<div class="item record">

	<!-- the following code identify the item title column on the browse item page -->
	
    <p> <?php 
		/* Michael Change start*/		
		$title = metadata('Item', array('MUMAC','Title'));
		
		if(!empty($title)){
			echo link_to_item($title);
		}
		
	?> </p>
	<!--<h2>< ?php echo $item ?></h2>-->
    
	
	<div class="item-meta">
	
	<!-- the following code first checks whether the item has a thumbnail, if it does
	the div classifies how the images is represented. -->
	
    <?php if (metadata('item', 'has thumbnail')): ?>
    <div class="item-img">
        <?php echo link_to_item(item_image('square_thumbnail')); ?>
    </div>
    <?php endif; ?>
	
	<!-- the following codes identify the item Description column on the browse item page -->
	<!--
    < ?php if ($description = metadata('item', array('Dublin Core', 'Description'), array('snippet'=>250))): ?>
    <div class="item-description">
        < ?php echo $description; ?>
    </div>
    < ?php endif; ?>
	-->
	
    <?php if (metadata('item', 'has tags')): ?>
    <div class="tags"><p><strong><?php echo __('Tags'); ?>:</strong>
        <?php echo tag_string('items'); ?></p>
    </div>
    <?php endif; ?>

    <?php fire_plugin_hook('public_items_browse_each', array('view' => $this, 'item' =>$item)); ?>
	</div> <!-- end class="item-meta" -->
</div><!-- end class="item hentry" -->
<?php endif?>
<?php endforeach; ?>
<br><br><br>
<?php echo pagination_links(); ?>
<!-- Disabled this as it was not making sense to why its there... TO BE REMOVED LATER
<div id="outputs">
    <span class="outputs-label">< ?php echo __('Output Formats'); ?></span> 
    < ?php echo output_format_list(false); ?>
</div>
-->
<?php fire_plugin_hook('public_items_browse', array('items'=>$items, 'view' => $this)); ?>

<?php echo foot(); ?>
