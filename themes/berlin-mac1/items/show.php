
<!-- This page is representation of each item -->

<!-- Michael Change-->
<?php echo head(array('title' => metadata('item', array('MUMAC', 'Title')),'bodyclass' => 'items show')); ?>

<div id="primary">
<!-- Michael Change Start -->
    

<div id="itemdiv">
    <!-- Items metadata -->
	<!-- Changes made by John -->
		<div id="itemLeft">
		<!-- Michael Change -->
		<?php $titles = metadata('Item', array('MUMAC','Title'),array('all'=>true)); ?>
		<?php foreach($titles as $title):	
			echo '<div id="item-metadata">';
			echo '<h1>', $title ,'</h1>';
			if (metadata('Item',array('MUMAC','Creator'))):
				echo '<p>',('Creator:'),metadata('Item',array('MUMAC','Culture')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Creator'))):
				echo '<p>',('Culture:'), metadata('Item',array('MUMAC','Culture')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Period'))):
				echo '<p>',('Period:'), metadata('Item',array('MUMAC','Period')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Date'))):
				echo '<p>',('Date:'), metadata('Item',array('MUMAC','Date')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Provenance'))):
				echo '<p>',('Provenance:'), metadata('Item',array('MUMAC','Provenance')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Location'))):
				echo '<p>',('Location:'), metadata('Item',array('MUMAC','Location')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Material'))):
				echo '<p>',('Material:'), metadata('Item',array('MUMAC','Material')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Classification'))):
				echo '<p>',('Classification:'),metadata('Item',array('MUMAC','Classification')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Preservation'))):
				echo '<p>',('Preservation:'), metadata('Item',array('MUMAC','Preservation')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Dimensions'))):
				echo '<p>',('Dimensions:'), metadata('Item',array('MUMAC','Dimensions')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Collection'))):
				echo '<p>',('Collection:'), metadata('Item',array('MUMAC','Collection')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Source'))):
				echo '<p>',('Source:'), metadata('Item',array('MUMAC','Source')),'</p>';
			endif;
			if (metadata('Item',array('MUMAC','Accession #/s'))):
				echo '<p>',('Accession #/s:'), metadata('Item',array('MUMAC','Accession #/s')),'</p>';
			endif;
			if (link_to_collection_for_item()):
				echo '<p> Museum: ', link_to_collection_for_item(),'</p>';
			endif;
			echo '</div>';
		endforeach ?>
		</div>
		
		<div id="itemRight">
			
				<?php if(metadata('item', array('MUMAC','3D'))):
					echo ('<div id="3dBox">');
					echo "<iframe class='3D'src=";
					echo str_replace("normal","plain",metadata('item', array('MUMAC','3D')));
					echo " frameborder='0' ></iframe>";
					echo '</div>';
					echo"<a href=";
					echo metadata('item', array('MUMAC','3D'));
					echo">View Full 3D Image</a>";
					
				else:
					echo ('<div id="3dBox" style="height:370px;">');
					echo item_image('square_thumbnail', array('class' => 'image'));
					echo ('</div>');
				endif ?>
			
			<div id="thumbnails">
				<!--
				< ?php echo item_image_gallery(
				  array('wrapper' => array('class' => 'gallery'),
				    'image' => array('class' => 'image')));
				?>
				< ?php
					echo item_image('square_thumbnail', array('class' => 'image')); ?>
				
				-->
				<?php echo files_for_item(
					array(
						'linkToFile' => false
					)
				); ?>

			</div>
			
		</div>
		
</div>
		<!-- Michael Change End -->
		<!-- Changes by JOHN -->
		<div id="tabs" style="width:100%">

		<ul>
		<?php if(metadata('Item',array('MUMAC', 'Description'))):
			echo '<li><a href="#tabs-1">Description</a></li>';
		endif?>
		
			<!--<li><a href="#tabs-1">Files</a></li>-->
		
		<?php if(metadata('Item',array('MUMAC', 'References'))):
			echo '<li><a href="#tabs-2">References</a></li>';
		endif?>
		<?php if(metadata('Item',array('MUMAC', 'Related Objects'))):
				echo '<li><a href="#tabs-3">Related</a></li>';
			 endif;
			if(metadata('Item',array('MUMAC', 'Parallels'))):
				echo '<li><a href="#tabs-3">Parallels</a></li>';
			endif?>
		
		<li><a href="#tabs-4">Resources</a></li>
		
		<?php if(metadata('item', 'has tags')):
			echo '<li><a href="#tabs-5">Tags</a></li>';
		endif?>
		<?php if(metadata('Item',array('MUMAC', 'Downloads'))):
			echo '<li><a href="#tabs-6">Downloads</a></li>';
		endif?>
		
		<li><a href="#tabs-7">Share</a></li>
		
		
		
		</ul>
	<?php if(metadata('Item', array('MUMAC','Description'))):
		echo '<div id="tabs-1">';
		echo '<h3> Description: </h3>',metadata('Item', array('MUMAC','Description'));
		echo '</div>';
	endif?>
	
	<?php if(metadata('Item',array('MUMAC', 'References'))):
		echo '<div id="tabs-2">';
		echo '<h3> References: </h3>',metadata('Item',array('MUMAC', 'References'));
		echo '</div>';
	endif?>
	<?php $test = metadata('Item',array('MUMAC', 'Related Objects'));
		$test1 = (metadata('Item',array('MUMAC', 'Parallels')));
	?>
	<?php if(empty($test) or empty($test1)) :
		echo '<div id="tabs-3">';

			if(metadata('Item',array('MUMAC', 'Related Objects'))):
				echo '<h3> Related Objects: </h3>',metadata('Item',array('MUMAC', 'Related Objects'));
			endif;
			if(metadata('Item',array('MUMAC', 'Parallels'))):
				echo '<h3> Parallels: </h3>', metadata('Item',array('MUMAC', 'Parallels'));
			endif;
		echo "</div>";
	endif?>

	
	<div id="tabs-4">
		<?php if(metadata('Item',array('MUMAC', 'Resources'))):
			if(current_user()):
				echo '<h3> Resources: </h3>',metadata('Item',array('MUMAC', 'Resources'));
				else:
					echo ("Please <a href='/admin' style='color:red'>Login</a> to access these contents");
			endif;
			else:
				echo ("This item has no Resources");
		endif?>
	</div>
	
	<div id="tabs-5">
		<!-- The following prints a list of all tags associated with the item -->
			<?php if (metadata('item','has tags')): ?>
				<h3><?php echo __('Tags'); ?></h3>
				<?php echo tag_string('item'); ?>
				<?php else:
					echo ('The Object has no tags'); ?>
			<?php endif;?>
	</div>
	<?php if(metadata('Item',array('MUMAC', 'Downloads'))):
		echo '<div id="tabs-6">';
			
				echo metadata('Item',array('MUMAC', 'Downloads'));
			
		echo '</div>';
	endif?>
	
	<div id="tabs-7">
		<h3><?php echo __('Citation'); ?></h3>
		<?php echo metadata('Item','citation',array('no_escape'=>true)); ?>
		<!-- The following prints and allows item details to be shared to social media. -->
		<?php fire_plugin_hook('public_items_show', array('view' => $this, 'item' => $item)); ?>
	</div>
	

</div>
	
<!-- end of John's changes -->
	<ul class="item-pagination navigation">
				<li id="previous-item" class="previous"><?php echo link_to_previous_item_show(); ?></li>
				<li id="next-item" class="next"><?php echo link_to_next_item_show(); ?></li>
			</ul>

</div> <!-- End of Primary. -->
<?php echo foot(); ?>