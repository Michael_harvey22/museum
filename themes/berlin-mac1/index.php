<?php echo head(array('bodyid'=>'home', 'bodyclass' =>'two-col')); ?>
<!-- C356 mac1: add Shrotcode Carousel-->
 
<p><?php echo $this->shortcodes('[carousel autoscroll=true interval=4]'); ?></p>




<div id="primary">

 <?php if ((get_theme_option('Display Featured Exhibit')) && function_exists('exhibit_builder_display_random_featured_exhibit')): ?>

    <!-- Featured Exhibit -->
    <!------------------------------------------------- ADDED BY EMEL - CODE START ------------------------------------------------------------------------>

    <?php /*echo exhibit_builder_display_random_featured_exhibit(); */ ?>

	<!------------------------------------------------- ADDED BY EMEL - CODE FINISH --------->
    
    <?php endif; ?>
    <?php
    $recentItems = get_theme_option('Homepage Featured Items');
    if ($recentItems === null || $recentItems === ''):
        $recentItems = 3;
    else:
        $recentItems = (int) $recentItems;
    endif;
    if ($recentItems):
    ?>
	<!--
    <div id="recent-items">
        <h2>< ?php echo __('Recently Added Items'); ?></h2>
        < ?php echo recent_items($recentItems); ?>
        <p class="view-items-link"><a href="<?php echo html_escape(url('items')); ?>"><?php echo __('View All Items'); ?></a></p>
    </div><!--end recent-items -->
    <?php endif; ?>

</div><!-- end primary -->

<div id="secondary">
    <!------------------------------------------------- ADDED BY EMEL - CODE START ------------------------------------------------------------------------>

     <?php if ($homepageText = get_theme_option('Homepage Text')): ?>
    <p><?php /*echo $homepageText;*/ ?></p>
    <?php endif; ?>
    <!------------------------------------------------- ADDED BY EMEL CODE - CODE FINISH ------------------------------------------------------------------------>          

       <!------------------------------------------------- ADDED BY EMEL - CODE START ------------------------------------------------------------------------>
<div class="well">
    <?php if (get_theme_option('Display Featured Collection')): ?>
    <!-- Featured Collection -->
    <div id="featured-collection" class="featured col-xs-6 col-sm-4">
        <h2><?php echo __('Featured Collection'); ?></h2>
        <?php echo random_featured_collection(); ?>
    </div><!-- end featured collection -->
    <?php endif; ?>

    <?php if (get_theme_option('Display Featured Item') == 1): ?>
    <!-- Featured Item -->
    <div id="featured-item" class="featured col-xs-6 col-sm-4">
        <h2><?php echo __('Featured Item'); ?></h2>
        <?php echo random_featured_items(1); ?>
    </div><!--end featured-item-->
    <?php endif; ?>

    <?php fire_plugin_hook('public_home', array('view' => $this)); ?>
     <!----------------------- ADDED by Emel - CODE START ------------------------------------------------>
         <?php echo exhibit_builder_display_random_featured_exhibit(); ?>

    
	<!---------------------- ADDED by Emel - CODE FINISH -------------------------------------------->
</div>
</div><!-- end secondary -->
<?php echo foot(); ?>
