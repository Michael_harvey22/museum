<?php
    $elementSetMetadata = array(
        'name'        => 'MUMAC',
        'description' => "MUMACElementSet",
        'record_type' => 'Item',
    );

    $elements = array(

        
        array(
            'label' => 'Title',
            'name'  => 'Title',
            'description' => 'Title',
            'data_type'   => 'Text',
        ),
	
	    array(
            'label' => 'Creator',
            'name'  => 'Creator',
            'description' => 'Creator',
            'data_type'   => 'Text',
        ),
        
		array(
            'label' => 'Culture',
            'name'  => 'Culture',
            'description' => 'Culture',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Period',
            'name'  => 'Period',
            'description' => 'Period',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Date',
            'name'  => 'Date',
            'description' => 'Date',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Provenance',
            'name'  => 'Provenance',
            'description' => 'Provenance',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Location',
            'name'  => 'Location',
            'description' => 'Location',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Material',
            'name'  => 'Material',
            'description' => 'Material',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Classification',
            'name'  => 'Classification',
            'description' => 'Classification',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Preservation',
            'name'  => 'Preservation',
            'description' => 'Preservation',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Dimensions',
            'name'  => 'Dimensions',
            'description' => 'Dimensions',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Collection',
            'name'  => 'Collection',
            'description' => 'Collection',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Source',
            'name'  => 'Source',
            'description' => 'Source',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Accession #/s',
            'name'  => 'Accession #/s',
            'description' => 'Accession #/s',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Description',
            'name'  => 'Description',
            'description' => 'Description',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Parallels',
            'name'  => 'Parallels',
            'description' => 'Parallels',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Related Objects',
            'name'  => 'Related Objects',
            'description' => 'Related Objects',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Resources',
            'name'  => 'Resources',
            'description' => 'Resources',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Downloads',
            'name'  => 'Downloads',
            'description' => 'Downloads',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'References',
            'name'  => 'References',
            'description' => 'References',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => 'Rights',
            'name'  => 'Rights',
            'description' => 'Rights',
            'data_type'   => 'Text',
        ),
		
		array(
            'label' => '3D',
            'name'  => '3D',
            'description' => '3D',
            'data_type'   => 'Text',
        ),
    );
