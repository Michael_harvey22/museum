<div class="jcarousel-wrapper">
    <div class="jcarousel" id="jcarousel-<?php echo $id_suffix; ?>">
        <ul>
        <?php foreach($items as $item): ?>
            <li>
            <div class="image" style="width:100%">
            <?php echo item_image('fullsize', array(), 0, $item);
            ?>
            </div>
            </li>
        <?php endforeach; ?>
        </ul>
    </div>

    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
    <a href="#" class="jcarousel-control-next">&rsaquo;</a>

    
</div>

<script type='text/javascript'>
var carouselConfig = <?php echo json_encode($configs['carousel']);?>;
var configs = <?php echo json_encode($configs);?>;
var carousel = jQuery('#jcarousel-<?php echo $id_suffix; ?>').jcarousel(carouselConfig);
<?php if(isset($configs['autoscroll'])): ?>
var autoscrollConfig = <?php echo json_encode($configs['autoscroll']);?>;
carousel.jcarouselAutoscroll(autoscrollConfig);
<?php endif; ?>
</script>
